# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import traceback
import paho.mqtt.client as mqtt
import socket
import struct
from ubidots import ApiClient
import logging.config

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

if is_rpi:
    import fcntl

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("tag")

param_names = {'ID': 'ID', 'X': 'ACC_x', 'Y': 'ACC_y', 'Z': 'ACC_z',
               'T': 'Temperature', 'H': 'Heart Rate', 'HV': 'Heart Rate Valid',
               'S': 'SPO2', 'SV': 'SPO2 Valid', 'LA': 'Latitude', 'LO': 'Longitude'}

param_units = {'ACC_x': ' °', 'ACC_y': ' °', 'ACC_z': ' °', 'Temperature': ' °F',
               'Heart Rate': ' N/sec', 'SPO2': ' %', 'Latitude': ' °', 'Longitude': ' °'}

api = ApiClient(token='ybQF2q5BVPQ1AU2p9o79WCcUN5a0ee')


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logger.debug("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("ear_tag")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    logger.debug('Message arrived : {}'.format(msg.payload))

    try:
        result = {}
        cur_ds = None
        for val in str(msg.payload).split(','):
            if val.split(':')[0] == 'ID':
                cur_ds = get_current_datasource(val.split(':')[1])
            else:
                result[param_names[val.split(':')[0]]] = float(val.split(':')[1])

        logger.debug('ID: {}'.format(cur_ds.name))
        # Pop invalid values
        if result['Heart Rate Valid'] != 1:
            logger.warning(' > Heart Rate is invalid')
            result.pop('Heart Rate', 0)
        if result['SPO2 Valid'] != 1:
            logger.warning(' > SPO2 is invalid')
            result.pop('SPO2', 0)
        for param in ['Temperature', 'ACC_x', 'ACC_y', 'ACC_z']:
            if result[param] == 0:
                logger.warning(' > {} is invalid'.format(param))
                result.pop(param, 0)
        for param in ['Latitude', 'Longitude']:
            if result[param] == 1000:
                logger.warning(' > {} is invalid'.format(param))
                result.pop(param, 0)
        result.pop('Heart Rate Valid', 0)
        result.pop('SPO2 Valid', 0)

        logger.debug(result)

        variables = cur_ds.get_variables()
        # print 'Current variables: ', variables
        collections = []
        for param in result.keys():
            if param in param_units.keys():
                # Add missing variables...
                if param not in [v.name for v in variables]:
                    try:
                        req = {"name": param, 'unit': param_units[param]}
                        logger.info('Missing variable, adding... {}'.format(req))
                        cur_ds.create_variable(req)
                        # Update variable list
                        variables = cur_ds.get_variables()
                    except ValueError:
                        logger.error('Failed to create a variable: {}'.format(param))
                        continue
                for var in variables:
                    if var.name == param:
                        collections.append({'variable': var.id, 'value': result[param]})

        # print 'Collections: ', collections
        api.save_collection(collections)
    except Exception:
        traceback.print_exc()


def get_current_datasource(_id):
    """
    Check datasource on Ubidots with given id and create new if not exists.
    :param _id:
    :return: datasource object
    """
    ds_list = api.get_datasources()
    if _id not in [d.name for d in ds_list]:
        req = {'name': _id, 'description': 'Ear Tag with ID: {}'.format(_id)}
        logger.warning('Cannot find datasource, creating new one... {}'.format(req))
        _new_ds = api.create_datasource(req)
        for _var in param_names.values():
            try:
                req = {"name": _var, 'unit': param_units[_var]}
                logger.debug('Adding variable... {}'.format(req))
                _new_ds.create_variable(req)
            except KeyError:
                pass
            except ValueError:
                logger.error('Failed to create a variable: {}'.format(_var))

        return _new_ds
    else:
        for ds in ds_list:
            if _id == ds.name:
                return ds


if __name__ == '__main__':

    if is_rpi:
        client = mqtt.Client()

        client.on_connect = on_connect
        client.on_message = on_message

        client.connect(get_ip_address('wlan0'), 1883, 60)

        # Blocking call that processes network traffic, dispatches callbacks and
        # handles reconnecting.
        # Other loop*() functions are available that give a threaded interface and a
        # manual interface.
        client.loop_forever()

    else:
        a = api.get_datasources()
        for aa in a:
            aaa = aa.get_variables()
            print aaa
        # new_ds = api.create_datasource({'name': 'test_sd', 'description': 'Ear Tag with ID: {}'.format('asdf')})
        # for var in ['v1', 'v2', 'v3']:
        #     new_ds.create_variable({"name": var, 'unit': var})
        # all_variables = api.get_variables()
        #
        # collection = []
        # for _var in all_variables:
        #     collection.append({'variable': _var.id, 'value': random.randint(1, 10)})
        # api.save_collection(collection)
        pass
