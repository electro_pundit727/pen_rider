/* ADXL345
  SDA = GPIO4(D2); SCL = GPIO5(D1)
 *
 */  

//////////////////////////////////////// ADXL345 ////////////////////////////////////////
#include <Wire.h>
#define DEVICE (0x53) // Device address as specified in data sheet
#define ADXL345_MG2G_MULTIPLIER (0.004)
#define SENSORS_GRAVITY_STANDARD          (SENSORS_GRAVITY_EARTH)
#define SENSORS_GRAVITY_EARTH             (9.80665F)              /**< Earth's gravity in m/s^2 */

byte adxl_buff[6];
float adxl_value[3];
float adxl_cal[3] = {0, 0, 0};

char POWER_CTL = 0x2D;    //Power Control Register
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    //X-Axis Data 0


void setup() {
  delay(1000);

  Serial.begin(9600);
  
  initialize_accel();
    
}

void loop() {

  // Read Accelerometer's values and display them.
  readAccel(true);  
  Serial.print("-- Accelerometer's values ->  X: ");Serial.print(adxl_value[0]);
  Serial.print("  Y: ");Serial.print(adxl_value[1]);
  Serial.print("  Z: ");Serial.println(adxl_value[2]);

  
  delay(1000);   

}

/*  -------------------------------- ADXL345 --------------------------------------------------- */
void initialize_accel(){
  Serial.println("-- Initializing ADXL345 module...");
  
  //ADXL345
  Wire.begin();      
  
  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  accel_write(DATA_FORMAT, 0x00);
  
  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  accel_write(POWER_CTL, 0x08);
  
  // Get 10 values and calculate calibration values.
  int i = 0;
  int j = 0;
  for(i = 0; i < 11; i++)
  {
    readAccel(false);
    for (j = 0; j < 3; j++){
		adxl_cal[j] += adxl_value[j];
	}
    delay(100);
  }
	
  for (j = 0; j < 3; j++){
		adxl_cal[j] /= 10;
	}
	
  Serial.print("-- Calibration values of ADXL345 ->  X: ");Serial.print(adxl_cal[0]);
  Serial.print("  Y: ");Serial.print(adxl_cal[1]);
  Serial.print("  Z: ");Serial.println(adxl_cal[2]);
  
}

// Read X, Y, Z values and store to "adxl_value" global variable...
void readAccel(bool cal) 
{
//  Serial.print("readAccel");
  uint8_t howManyBytesToRead = 6; //6 for all axes
  accel_read( DATAX0, howManyBytesToRead, adxl_buff); //read the acceleration data from the ADXL345
  short x = 0;
  x = (((short)adxl_buff[1]) << 8) | adxl_buff[0];
  adxl_value[0] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[3]) << 8) | adxl_buff[2];
  adxl_value[1] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[5]) << 8) | adxl_buff[4];
  adxl_value[2] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;

  if (cal == true)
    for (int j = 0; j < 3; j++){
      adxl_value[j] -= adxl_cal[j];
    }
}

void accel_write(byte address, byte val) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void accel_read(byte address, int num, byte _buff[]) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}

