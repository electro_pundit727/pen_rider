#include <ESP8266WiFi.h>
#include <Wire.h>
#include <Adafruit_MLX90614.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

/////////////////////////////////// WiFi Definitions ////////////////////////////
const char WiFiAPPSK[] = "penrider";
WiFiServer server(80);
/////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////// MLX96014 Temperature Sensor  ///////////////
Adafruit_MLX90614 mlx = Adafruit_MLX90614();
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////// ADXL345 Accelerometer ///////////////////////
#include <Wire.h>
#define DEVICE (0x53) // Device address as specified in data sheet
#define ADXL345_MG2G_MULTIPLIER (0.004)
#define SENSORS_GRAVITY_STANDARD          (SENSORS_GRAVITY_EARTH)
#define SENSORS_GRAVITY_EARTH             (9.80665F)              /**< Earth's gravity in m/s^2 */

byte adxl_buff[6];
float adxl_value[3];
float adxl_cal[3] = {0, 0, 0};

char POWER_CTL = 0x2D;    
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    
/////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////// GPS ///////////////////////////////////////
TinyGPS gps;
SoftwareSerial ss(D6, D5);

static void smartdelay(unsigned long ms);
static void print_float(float val, float invalid, int len, int prec);
static void print_int(unsigned long val, unsigned long invalid, int len);
static void print_date(TinyGPS &gps);
static void print_str(const char *str, int len);
/////////////////////////////////////////////////////////////////////////////////


void setup() 
{
  initHardware();
  setupWiFi();
  server.begin();
  mlx.begin();  
  initialize_accel();
}

void loop() 
{
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    readAccel(true);
    read_gps(); 
    return;
  }

  client.flush();

  // Prepare the response. Start with the common header:
  String s = "HTTP/1.1 200 OK\r\n";
  s += "Content-Type: text/html\r\n\r\n";
  s += "<!DOCTYPE HTML>\r\n<html>\r\n";
  
  // Content of HTML page
  s += "<br>----- Temperature -----<br>"; 
  s += " Ambient : "; s += String(mlx.readAmbientTempF()); s += "<br>"; 
  s += " Object : "; s += String(mlx.readObjectTempF()); s += "<br>"; 
  
  readAccel(true);
  s += "<br>----- Accelerometer Values -----<br>"; 
  s += " X : "; s += String(adxl_value[0]); s += "<br>"; 
  s += " Y : "; s += String(adxl_value[1]); s += "<br>"; 
  s += " Z : "; s += String(adxl_value[2]); s += "<br>"; 
  
  float flat, flon;
  unsigned long age, date, time, chars = 0;
  unsigned short sentences = 0, failed = 0;
  static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;
  s += "<br>----- GPS Values -----<br>"; 
  read_gps();
  if (gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES)
    s += " Satellite : ********<br>"; 
  else
    s += " Satellite : "; s += String(gps.satellites()); s += "<br>"; 
  gps.f_get_position(&flat, &flon, &age);
  if (flat == TinyGPS::GPS_INVALID_F_ANGLE)
    s += " Latitude : ********<br>"; 
  else
    s += " Latitude : "; s += String(flat); s += "<br>";   
  if (flon == TinyGPS::GPS_INVALID_F_ANGLE)
    s += " Longitude : ********<br>"; 
  else
    s += " Longitude : "; s += String(flon); s += "<br>";   
  if (age == TinyGPS::GPS_INVALID_AGE)
    s += " Age : ********<br>";  
  else
    s += " Age : "; s += String(age); s += "<br>"; 
  
  int year;
  byte month, day, hour, minute, second, hundredths;
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  if (age == TinyGPS::GPS_INVALID_AGE)
    s += " Date & Time: ********** ********<br>";
  else{
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d %02d:%02d:%02d ", month, day, year, hour, minute, second);  
    s += " Date & Time: "; s += String(sz); s += "<br>";    
  }
 
  if (gps.f_altitude() == TinyGPS::GPS_INVALID_F_ALTITUDE)
    s += " Altitude : ********<br>"; 
  else
    s += " Altitude : "; s += String(gps.f_altitude()); s += "<br>";    
  
  s += "</html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println(s);

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}

void setupWiFi()
{
  WiFi.mode(WIFI_AP);
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                 String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macID.toUpperCase();
  String AP_NameString = "PEN RIDER " + macID;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);

  for (int i=0; i<AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  WiFi.softAP(AP_NameChar, WiFiAPPSK);
}

void initHardware()
{
  Serial.begin(9600);
  // Don't need to set ANALOG_PIN as input, 
  // that's all it can be.
}

/*  -------------------------------- ADXL345 --------------------------------------------------- */
void initialize_accel(){
  Serial.println("-- Initializing ADXL345 module...");
  
  //ADXL345
  // i2c bus SDA = GPIO0; SCL = GPIO2
  Wire.begin();      
  
  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  accel_write(DATA_FORMAT, 0x00);
  
  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  accel_write(POWER_CTL, 0x08);
  
  // Get 10 values and calculate calibration values.
  int i = 0;
  int j = 0;
  for(i = 0; i < 11; i++)
  {
    readAccel(false);
    for (j = 0; j < 3; j++){
    adxl_cal[j] += adxl_value[j];
  }
    delay(100);
  }
  
  for (j = 0; j < 3; j++){
    adxl_cal[j] /= 10;
  }
  
//  Serial.print("-- Calibration values of ADXL345 ->  X: ");Serial.print(adxl_cal[0]);
//  Serial.print("  Y: ");Serial.print(adxl_cal[1]);
//  Serial.print("  Z: ");Serial.println(adxl_cal[2]);
  
}

// Read X, Y, Z values and store to "adxl_value" global variable...
void readAccel(bool cal) 
{
//  Serial.print("readAccel");
  uint8_t howManyBytesToRead = 6; //6 for all axes
  accel_read( DATAX0, howManyBytesToRead, adxl_buff); //read the acceleration data from the ADXL345
  short x = 0;
  x = (((short)adxl_buff[1]) << 8) | adxl_buff[0];
  adxl_value[0] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[3]) << 8) | adxl_buff[2];
  adxl_value[1] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[5]) << 8) | adxl_buff[4];
  adxl_value[2] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;

  if (cal == true)
    for (int j = 0; j < 3; j++){
      adxl_value[j] -= adxl_cal[j];
    }
}

void accel_write(byte address, byte val) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void accel_read(byte address, int num, byte _buff[]) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}

void read_gps(){
  while (ss.available())
    gps.encode(ss.read());
}


