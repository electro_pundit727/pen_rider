"""
    LoRa Listener script

    Usage:
        sudo ./lora_gateway  | python lora_listener.py

    Expected prefixes

     ^p    indicates a ctrl pkt info ^pdst(%d),ptype(%d),src(%d),seq(%d),len(%d),SNR(%d),RSSI=(%d)
            ex: ^p1,16,3,0,234,8,-45

     ^r    indicate a ctrl radio info ^rbw,cr,sf for the last received packet
            ex: ^r500,5,12

     !#    indicates a message that should be uploaded to ubidots
            ex: !#ID:TAG_012345,X:0.39,Y:0.50,Z:9.61,T:74.31,H:150,HV:1,S:51,SV:1,LA:44.273836,LO:-96.897056
     \xFF\xFE        indicates radio data prefix

"""
import datetime
import os
import sys
import logging.config
import ubidot_ctrl

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except IOError:
    print 'Failed to load configuration of logging'

logger = logging.getLogger("tag")

app_key_list = [
    # change here your application key
    [1, 2, 3, 4],
    [5, 6, 7, 8]
]

# header packet information
HEADER_SIZE = 4
PKT_TYPE_DATA = 0x10
PKT_TYPE_ACK = 0x20

PKT_FLAG_ACK_REQ = 0x08
PKT_FLAG_DATA_ENCRYPTED = 0x04
PKT_FLAG_DATA_WAPPKEY = 0x02
PKT_FLAG_DATA_ISBINARY = 0x01

# last pkt information
dst = 0
ptype = 0
ptypestr = "N/A"
src = 0
seq = 0
datalen = 0
SNR = 0
RSSI = 0
bw = 0
cr = 0
sf = 0

# will ignore lines beginning with '?'
_ignoreComment = 1

_gwaddr = 1

_wappkey = 0
the_app_key = [0, 0, 0, 0]

# valid app key? by default we do not check for the app key
_validappkey = 1


if __name__ == '__main__':
    while True:
        sys.stdout.flush()
        ch = sys.stdin.read(1)

        # '^' is reserved for control information from the gateway
        if ch == '^':
            ch = sys.stdin.read(1)

            if ch == 'p':
                data = sys.stdin.readline()
                print ("rcv ctrl pkt info (^p): {}".format(data))
                arr = map(int, data.split(','))
                dst = arr[0]
                ptype = arr[1]
                ptypestr = "N/A"
                if (ptype & 0xF0) == PKT_TYPE_DATA:
                    ptypestr = "DATA"
                    if (ptype & PKT_FLAG_DATA_ISBINARY) == PKT_FLAG_DATA_ISBINARY:
                        ptypestr += " IS_BINARY"
                    if (ptype & PKT_FLAG_DATA_WAPPKEY) == PKT_FLAG_DATA_WAPPKEY:
                        ptypestr += " WAPPKEY"
                    if (ptype & PKT_FLAG_DATA_ENCRYPTED) == PKT_FLAG_DATA_ENCRYPTED:
                        ptypestr += " ENCRYPTED"
                    if (ptype & PKT_FLAG_ACK_REQ) == PKT_FLAG_ACK_REQ:
                        ptypestr += " ACK_REQ"
                if (ptype & 0xF0) == PKT_TYPE_ACK:
                    ptypestr = "ACK"
                src = arr[2]
                seq = arr[3]
                datalen = arr[4]
                SNR = arr[5]
                RSSI = arr[6]

                info_str = "(dst=%d type=0x%.2X(%s) src=%d seq=%d len=%d SNR=%d RSSI=%d)" % (
                            dst, ptype, ptypestr, src, seq, datalen, SNR, RSSI)

                logger.debug(info_str)

            if ch == 'r':
                data = sys.stdin.readline()
                print ("rcv ctrl radio info (^r): {}".format(data))
                arr = map(int, data.split(','))
                bw = arr[0]
                cr = arr[1]
                sf = arr[2]
                info_str = "(BW=%d CR=%d SF=%d)" % (bw, cr, sf)
                logger.debug(info_str)

            if ch == 'l':
                # TODO: LAS service
                print 'not implemented yet'

            continue

        # '\' is reserved for message logging service
        if ch == '\\':
            now = datetime.datetime.now()
            rcv = sys.stdin.readline()
            logger.info("Received data: {}".format(rcv))
            if rcv.startswith("!#"):
                logger.debug("Uploading to Ubidots...")
                ubidot_ctrl.upload(rcv[2:])
            else:  # not a known data logging prefix
                logger.warn('unrecognized data')
                sys.stdin.readline()

            continue

        # handle binary prefixes
        # if (ch == '\xFF' or ch == '+'):
        if ch == '\xFF':

            print "got first framing byte"
            ch = sys.stdin.read(1)
            # if (ch == '\xFE' or ch == '('):
            if ch == '\xFE':
                print "--> got data prefix"

        if ch == '?' and _ignoreComment == 1:
            sys.stdin.readline()
            continue

        sys.stdout.write(ch)
